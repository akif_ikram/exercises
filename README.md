Some Ideas & Interview Questions
================================

Some thoughts and ideas that I've discussed with friends, or have run into
during interviews.

For instance, friends have mentioned the "power" function.  It is a favorite
in many interviews, and the accepted solution in optimized form is in `power.py`
as the `power2` function. As I wrote that, I wondered why stop at 2? Why not go
n % 4, or n % 8, or even higher. My experiment (not super controlled, but still
mostly valid, IMO) shows that when you go from `power2` to `power4`, the number of
multiplications goes up, but (I assume) because you lower the recursion depth,
power4 will win out over power2 in most cases.

Or, the fibonacci3 function. I assume that's asked to test whether the candidate
knows recursion.  However, just looking at the answer, you can see that it will
likely have performance issues. By the time I call fibonacci3(30) or so, it takes
several seconds to run, and when I call fibonacci3(50) or higher, it goes into
minutes.  Using my alternative solution, you use a little bit of space; a "small"
array, but even going up to fibonacci3(1000), returns in microseconds.

#### Profiling information from running `fibonacci3.py`
```
786000372 function calls (606 primitive calls) in 168.788 seconds

Ordered by: standard name
```
ncalls | tottime | percall | cumtime | percall | filename:lineno(function)
---|---|---|---|---|---
1  |  0.001  |  0.001 | 168.788 | 168.788 | fibonacci3.py:18(<module>)
785999829/63 | 168.785 |   0.000 | 168.785  |  2.679 | fibonacci3.py:20(fibonacci3)
1   | 0.000  |  0.000  |  0.000  |  0.000 | fibonacci3.py:28(looked_up_fibonacci)
63  |  0.000  |  0.000  |  0.000  |  0.000 | fibonacci3.py:31(fibonacci3_2)
1  |  0.000   | 0.000  |  0.000  |  0.000 | {built-in method builtins.__build_class__}
1  |  0.000  |  0.000  | 168.788 | 168.788 | {built-in method builtins.exec}
56 |   0.000   | 0.000  |  0.000  |  0.000 | {built-in method builtins.len}
135 |   0.001  |  0.000  |  0.001  |  0.000 | {built-in method builtins.print}
252  |  0.000  |  0.000  |  0.000   | 0.000 | {built-in method time.process_time}
32  |  0.000  |  0.000   | 0.000  |  0.000 | {method 'append' of 'list' objects}
1   | 0.000 |   0.000 |   0.000  |  0.000 | {method 'disable' of '_lsprof.Profiler' objects}
