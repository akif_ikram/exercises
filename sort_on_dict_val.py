d = {
        'a': [(0,1),(2,3),(1,6)],
        'b': [(1,2),(1,2)],
        'c': [(1,1)],
        'd': [(4,5),(3,4)],
    }

def srtd (d):
    """
    Short and sweet
    """
    return sorted(d,key=lambda x: len(d[x]))

def srtd2 (d):
    """
    Easier to understand what's going on, and more functionality, such as
    secondary sort, etc. can be added as needed more easily.
    """
    sv = []
    for k,v in d.items():
        sv.append ((len(v), k))

    return [x[1] for x in sorted(sv)]

def srtd3 (d):
    """
    Sort on number of tuples, and for dupes, do secondary sort on sum of 1st
    tuple. Using "sorted"
    """
    y = sorted(d,key=lambda x: -sum(d[x][0]))
    return sorted(y,key=lambda x: len(d[x]))

def srtd4 (d):
    """
    Sort on number of tuples, and for dupes, do secondary sort on sum of 1st
    tuple. Using loop.
    """
    sv = []
    for k,v in d.items():
        sv.append ((len(v), -sum(v[0]), k))

    return [x[2] for x in sorted(sv)]

print (srtd(d))
print (srtd2(d))
print (srtd3(d))
print (srtd4(d))
