import random
def partition(list, left, right, pivotIndex):
    pivotValue = list[pivotIndex]

    # Swap pivot point with the right value
    tmp = list[right]
    list[right] = list[pivotIndex]
    list[pivotIndex] = tmp

    storeIndex = left
    for i in range(left,right):
        if list[i] < pivotValue:
            tmp = list[i]
            list[i] = list[storeIndex]
            list[storeIndex] = tmp
            storeIndex += 1

    # Swap pivot point  back to it's place
    tmp = list[right]
    list[right] = list[storeIndex]
    list[storeIndex] = tmp

    return storeIndex

# // Returns the k-th smallest element of list within left..right inclusive
# // (i.e. left <= k <= right).
# // The search space within the array is changing for each round - but the list
# // is still the same size. Thus, k does not need to be updated with each round.
def select(list, left, right, k):
    #print (left, right, k, list)
    if left == right:      #  // If the list contains only one element,
        return list[left]  # // return that element
    pivotIndex = random.randrange(left,right) #...     #// select a pivotIndex between left and right,
                        #// e.g., left + floor(rand() % (right - left + 1))
    pivotIndex = partition(list, left, right, pivotIndex)
    #print (left, right, k, list)
    # // The pivot is in its final sorted position
    if k == pivotIndex:
        return list[k]
    elif k < pivotIndex:
        return select(list, left, pivotIndex - 1, k)
    else:
        return select(list, pivotIndex + 1, right, k)

x = [10,11,12,13,14,15,1,2,3,4,5,6,7,8,9]
print ('-> ' + str(len(x)))
print ('-> ' + str(x))
#print ('.. ' + str(partition (x, 0, 14, 6)))
print (select (x, 0, 14, 5-1))
print ('-> ' + str(x))
