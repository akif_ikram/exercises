def unique_values (arr):
    """
    Input array has sorted non-unique values, return array with only unique
    values - using no data structures except variables.
    """
    u = 1
    curr_elem = arr[0]
    for i in range(1,len(arr)):
        if arr[i] != curr_elem:
            curr_elem = arr[i]
            arr[u] = curr_elem
            u += 1

    return arr[:u]

print (unique_values([1,1,1,2,3,3,4,4,4,4,5]))
