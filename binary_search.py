def binary_search (arr, sv):
    """
    Given a sorted array and a search value, implement "find".

    Best option for searching sorted values is binary search.
    """
    if len (arr) == 0:
        return (False, None)

    m = len(arr)/2
    if arr[m] == sv:
        return (True, m)
    elif arr[m] < sv:
        # if you don't care about the location, just do a simple return
        st, ind =  binary_search(arr[m+1:], sv)
        if st:
            return (st, m+1+ind)
        else:
            return (st, ind)
    else:
        return binary_search(arr[:m], sv)


t_arr = [
       ([1,2,6,8,9,15,25],8),
        ([1,2,6,8,9,15,25],9),
        ([1,2,6,8,9,15,25],25),
        ([1,2,6,8,9,15,25],0),
        ([1,2,6,8,9,11,15,17,25,30],8),
        ([1,2,6,8,9,11,15,17,25,30],30),
    ]
for x in t_arr:
    (status, ind) = binary_search (x[0], x[1])
    print (x, status, ind)
