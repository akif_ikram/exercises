def matching_braces (in_str):
    """
    Check if given string is balanced with regards to brackets, ( [ { } ] )

    Making 'check_options' allows adding any additional matching characters
    as needed.
    """
    check_options = {
            '[': ']',
            '(': ')',
            '{': '}',
        }
    s = [] # stack
    BALANCED = 'balanced'
    UNBALANCED = 'unbalanced'

    for i in in_str:
        if i in check_options:
            s.append(i)
        elif i in check_options.values():
            try:
                saved_val = s.pop()
                if check_options[saved_val] != i:
                    return UNBALANCED
            except IndexError:
                return UNBALANCED

    if len(s) == 0:
        return BALANCED
    else:
        return UNBALANCED

t_arr = [
        '( h {e} [ll] o)',
        '( th {e [r] e)',
        '(',
        ')',
        '()[]{}()'
    ]
for x in t_arr:
    print (x + ' : ' + matching_braces(x))
