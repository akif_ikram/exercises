def highest_value_string(in_str, k):
    """
    find string that returns biggest number, while removing k elements
    '14216', k=1 -> '4216', k=2 -> '426'  ... remove digit if next one is higher
    """
    ret_str = ''
    for i in range(1,len(in_str)):
        print (ret_str, in_str[i-1], in_str[i],k)
        if in_str[i-1] <= in_str[i] and k > 0:
            k -= 1
        else:
            ret_str += in_str[i-1]
    if k == 0:
        ret_str += in_str[-1]

    return ret_str


print (highest_value_string('114261',1))
print (highest_value_string('114261',2))
print (highest_value_string('114261',3))
print (highest_value_string('114216',4))
print (highest_value_string('1114261',4))
