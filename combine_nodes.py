def combine_nodes (lst, n1, n2):
    """
    Given a double-linked list, Create a new node, combining 2 consecutive
    nodes into one; average the value.

    Ensure the given nodes are contigous.
    """

    # Check they are contigious, and figure out which one is first, and which
    # second.  If they aren't contigious, fail
    if n1.next == n2:
        f = n1
        l = n2
    elif n2.next == n1:
        f = n2
        l = n1
    else:
        return (False)

    # No need to create a new node, use the first one as the "new" node.
    f.val = float(f.val + l.val)/2

    # Update "next" pointer to bypass the old 2nd node
    f.next = l.next

    # If the 2nd node isn't the last one in the list, update the pointer of the
    # next node (not given) to point to the correct one.
    if l.next:
        l.next.prev = f

    return (True)


class Node:
    def __init__ (self, name, val, prev=None, next=None):
        self.prev = prev
        self.next = next
        self.val = val
        self.name = name

    def __str__ (self):
        return '{:8} {:4}:{:5.2f} {:10}'.format(
            ('' if self.prev == None else self.prev.name + ' <-  '),
            self.name, self.val,
            ('' if self.next == None else '  -> ' + self.next.name)
        )

def print_list(l):
    n = l
    while n != None:
        print (n)
        n = n.next

# setup a list
n1 = Node('n1', 2)
n2 = Node('n2', 4,prev=n1)
n3 = Node('n3', 6,prev=n2)
n4 = Node('n4', 8,prev=n3)
n1.next = n2
n2.next = n3
n3.next = n4
l = n1

print_list(l)

if combine_nodes(l,n1,n3):
    print ('combined: n1, n3')
else:
    print ('failed: n1, n3')
print_list(l)

if combine_nodes(l,n4,n3):
    print ('combined: n4, n3')
else:
    print ('failed: n4, n3')
print_list(l)


# setup a list
n1 = Node('n1', 1)
n2 = Node('n2', 2,prev=n1)
n3 = Node('n3', 3,prev=n2)
n4 = Node('n4', 4,prev=n3)
n1.next = n2
n2.next = n3
n3.next = n4
l = n1

print ("=====================================================")
print_list(l)

if combine_nodes(l,n1,n2):
    print ('combined: n1, n2')
else:
    print ('failed: n1, n2')
print_list(l)

if combine_nodes(l,n4,n3):
    print ('combined: n4, n3')
else:
    print ('failed: n4, n3')
print_list(l)

if combine_nodes(l,n3,n1):
    print ('combined: n3, n1')
else:
    print ('failed: n3, n1')
print_list(l)

if combine_nodes(l,n4,n1):
    print ('combined: n4, n1')
else:
    print ('failed: n4, n1')
print_list(l)
