class top3list(list):
    """
    Use case is that data is mostly inserted into the list, and we need the
    largest 3 numbers often.

    Extends list class.

    Implements functionality so that the largest 3 elements of a list are always
    accessible by attributes. "add", etc. add minimal processing, but "remove"
    in some cases (element removed is one of top 3) will traverse the list
    to ensure the top 3 are accurate.

    Create list as 'x = top3list()', or 'x = top3list(1)', or
        'x = top3list([1,2])'

    Add items by 'x.append(4)', or 'x.append([5,6])'

    Remove items by 'x.remove(4)'
    """
    def __init__ (self, item=None):
        self.highest = -1
        self.second = -1
        self.third = -1
        if item:
            self.append(item)

    def append(self,item):
        if type(item) is int:
            list.append(self, item)
            if item > self.highest:
                self.third   = self.second
                self.second  = self.highest
                self.highest = item
            elif item > self.second:
                self.third   = self.second
                self.second  = item
            elif item > self.third:
                self.third   = item
        elif type(item) is list:
            for x in item:
                self.append(x)

    def extend(self, item):
        self.append(item)

    def remove(self,item):
        list.remove(self,item)
        # can this be done more efficiently?
        if item >= self.third:
            self.highest = -1
            self.second = -1
            self.third = -1
            for i in self:
                if i > self.highest:
                    self.third   = self.second
                    self.second  = self.highest
                    self.highest = i
                elif i > self.second:
                    self.third   = self.second
                    self.second  = i
                elif i > self.third:
                    self.third   = i


#x = mylist(1)
#y = mylist([1,4])
#print ('x', x.highest, x.second, x.third, x.fourth, x.data)
#print ('y', y.highest, y.second, y.third, y.fourth, y.data)
#x.append([5,10])
y = top3list([1,4,3,10])
print (y)
print ('y', y.highest, y.second, y.third, y)
y.append(99)
print ('y', y.highest, y.second, y.third, y)
y.append([45,53])
print ('y', y.highest, y.second, y.third, y)
y.extend(100)
print ('y', y.highest, y.second, y.third, y)
y.extend([101])
print ('y', y.highest, y.second, y.third, y)
y.remove(45)
print ('y', y.highest, y.second, y.third, y)
y.remove(99)
print ('y', y.highest, y.second, y.third, y)
y.remove(3)
print ('y', y.highest, y.second, y.third, y)
print(top3list.__doc__)
