"""
Function to return a Fibonacci3 number (after the start, it is the sum of the
last 3 numbers).

1, 1, 1, 3, 5, 9, 17, 31, ...

The first function is a simple recursive function.

The second solution is more optimized, with a class attribute.  Fixes the
obvious problem that for any given "n" (for the first function), you
calculate f(n-3) 3 times, f(n-2) 2 times, and f(n-1) once - all from scratch.

By saving calculated values, you get a much improved response time.  You can
see yourself:

python3 fibonacci3.py

"""

def fibonacci3 (n):
    if n <= 0:
        return 0
    elif n <= 3:
        return 1
    else:
        return fibonacci3(n-3) + fibonacci3(n-2) + fibonacci3(n-1)

class looked_up_fibonacci:
    ser = [1, 1, 1]

def fibonacci3_2 (n):
    if n <= 0:
        return 0
    elif n <= 3:
        return 1
    else:
        if len(looked_up_fibonacci.ser) >= n:
            return looked_up_fibonacci.ser[n-1]
        else:
            l = len(looked_up_fibonacci.ser)
            while l < n:
                next_nbr = looked_up_fibonacci.ser[l-3] + looked_up_fibonacci.ser[l-2] + looked_up_fibonacci.ser[l-1]
                looked_up_fibonacci.ser.append(next_nbr)
                l += 1
            return looked_up_fibonacci.ser[-1]

import time
import sys

print ("========================================")
print ("Single call: recursive")
x = 35
s = time.process_time()
f = fibonacci3(x)
e = time.process_time()
print (f'f({x:2}): {f:10} {(e-s)*1000000:16,.4f} microseconds' )

print ("Single call: iterative")
s = time.process_time()
f = fibonacci3_2(x)
e = time.process_time()
print (f'f({x:2}): {f:10} {(e-s)*1000000:16,.4f} microseconds' )

print ("========================================")


upper_limit = 31

for x in range(upper_limit):
    s = time.process_time()
    f = fibonacci3(x)
    e = time.process_time()
    print (f'f({x:2}): {f:10} {(e-s)*1000000:16,.4f} microseconds' )

print ("========================================")
for x in range(upper_limit):
    s = time.process_time()
    f = fibonacci3_2(x)
    e = time.process_time()
    print (f'f({x:2}): {f:10} {(e-s)*1000000:16,.4f} microseconds' )

# Reverse
print ("========================================")
for x in range(upper_limit-1,-1,-1):
    s = time.process_time()
    f = fibonacci3(x)
    e = time.process_time()
    print (f'f({x:2}): {f:10} {(e-s)*1000000:16,.4f} microseconds' )

print ("========================================")
for x in range(upper_limit-1,-1,-1):
    s = time.process_time()
    f = fibonacci3_2(x)
    e = time.process_time()
    print (f'f({x:2}): {f:10} {(e-s)*1000000:16,.4f} microseconds' )

print ("========================================")
print ('series', looked_up_fibonacci.ser)
