def process_csv_input (in_str):
    """
    Take an input string, and break it down into an array:

    Array should start with 'START', and end with 'END'.  The values
    between START and END should be taken from the string.

    Values are comma separated, and can optionally be enclosed in single
    quotes '.

    Code using only basic language functionality, not regex, etc.
    """
    arr = in_str.split(',')
    clean_arr = ['START']
    i = 0
    while i <  len(arr):
        if arr[i].startswith ("'"):
            if arr[i].endswith("'"):
                clean_arr.append(arr[i])
            else:
                strx = arr[i]
                i = i+1
                done = False
                while not done:
                    strx += "," + arr[i]
                    if arr[i].endswith("'"):
                        done = True
                    i += 1

                i -= 1

                clean_arr.append(strx)
        else:
            clean_arr.append(arr[i])

        i += 1

    clean_arr.append('END')
    return clean_arr

in_str = "'a',b,'x,y,z',,input_string"
output = process_csv_input(in_str)
print (output)
