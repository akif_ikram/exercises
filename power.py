# Testing the different power functions.
#
# Power4 seems to win almost every time.
#
# Tested with:
# python3 power.py > out.txt; cut -c22-27 out.txt | sort | uniq -c | sort -rn

def power(b, e):
    """
    Basic power function that will handle positive and negative exponent.

    Code is written to get back the number of multiplications, hence the (i,x)
    and (i,y) calls. Unoptimized would have made two calls to power for each
    operand.
    """
    if e == 0:
        return (0, 1)
    elif e == 1:
        return (0, b)
    elif e < 0:
        (i, x) = power(b, -e)
        return (i, float(1/x))
    elif e % 2 == 0:  # even
        (i, x) = power(b, int(e/2))
        (j, y) = power(b, int(e/2))
        return (i+j+1, x * y)
    else:
        (i, x) = power(b, int(e/2))
        (j, y) = power(b, int(e/2))
        return (i+j+2, b * x * y)

def power2(b, e):
    """
    Optimized power function that will handle positive and negative exponents
    """
    if e == 0:
        return (0, 1)
    elif e == 1:
        return (0, b)
    elif e < 0:
        (i, x) = power2(b, -e)
        return (i, float(1/x))
    elif e % 2 == 0:  # even
        (i, x) = power2(b, int(e/2))
        return (i+1, x * x)
    else:
        (i, x) = power2(b, int(e/2))
        return (i+2, b * x * x)

def power4(b, e):
    """
    Optimized power function that will handle positive and negative exponents.

    Testing counts of total multiplications vs. number of recursive calls
    """
    if e == 0:
        return (0, 1)
    elif e == 1:
        return (0, b)
    elif e < 0:
        (i, x) = power4(b, -e)
        return (i, float(1/x))
    elif e % 4 == 0:
        (i, x) = power4(b, int(e/4))
        return (i+3, x * x * x * x)
    elif e % 2 == 0:
        (i, x) = power4(b, int(e/2))
        return (i+1, x * x)
    else:
        (i, x) = power4(b, int(e/2))
        return (i+2, b * x * x)

def power8(b, e):
    """
    Optimized power function that will handle positive and negative exponents.

    Testing counts of total multiplications vs. number of recursive calls
    """
    if e == 0:
        return (0, 1)
    elif e == 1:
        return (0, b)
    elif e < 0:
        (i, x) = power8(b, -e)
        return (i, float(1/x))
    elif e % 8 == 0:  # even
        (i, x) = power8(b, int(e/8))
        return (i+7, x * x * x * x * x * x * x * x)
    elif e % 4 == 0:  # even
        (i, x) = power8(b, int(e/4))
        return (i+3, x * x * x * x)
    elif e % 2 == 0:  # even
        (i, x) = power8(b, int(e/2))
        return (i+1, x * x)
    else:
        (i, x) = power8(b, int(e/2))
        return (i+2, b * x * x)


import time
import sys

if sys.version_info[0] >= 3:
    py3 = True
else:
    py3 = False

run_info = {}
t_arr = [(2,i) for i in range(1024,2048,1)]

# for x in t_arr:
#     print (power(x[0],x[1]))

#t_arr = [(2,i) for i in range(-1024,1024,1)]

for x in t_arr:
    run_info[x] = {'time':{}, 'multiplications': {}}

    s = time.process_time()
    (m, r) = power(x[0],x[1])
    e = time.process_time()
    run_info[x]['time']['power'] = e-s
    run_info[x]['multiplications']['power'] =  m
    #print (f'power:  {x[0]}^{x[1]}, *s: {m:3}, result: {r}, microsecs:{(e-s)*1000000:8.4f}')

    s = time.process_time()
    (m, r) = power2(x[0],x[1])
    e = time.process_time()
    run_info[x]['time']['power2'] = e-s
    run_info[x]['multiplications']['power2'] =  m
    #print (f'power2: {x[0]}^{x[1]}, *s: {m:3}, result: {r}, microsecs:{(e-s)*1000000:8.4f}')

    s = time.process_time()
    (m, r) = power4(x[0],x[1])
    e = time.process_time()
    run_info[x]['time']['power4'] = e-s
    run_info[x]['multiplications']['power4'] =  m
    #print (f'power4: {x[0]}^{x[1]}, *s: {m:3}, result: {r}, microsecs:{(e-s)*1000000:8.4f}')

    s = time.process_time()
    (m, r) = power8(x[0],x[1])
    e = time.process_time()
    run_info[x]['time']['power8'] = e-s
    run_info[x]['multiplications']['power8'] =  m
    #print (f'power8: {x[0]}^{x[1]}, *s: {m:3}, result: {r}, microsecs:{(e-s)*1000000:8.4f}')

#print (minimums)
#print (run_info)
for k,v in run_info.items():
    (n1, t) = (sorted(v['time'].items(), key=lambda kv: kv[1])[0])
    (n3, t2) = (sorted(v['time'].items(), key=lambda kv: kv[1], reverse=True)[0])
    (n2, m) = (sorted(v['multiplications'].items(), key=lambda kv: kv[1])[0])
    print ( f'{k[0]:3}^{k[1]:5} {t*1000000:8.4f} - {n1:6} / {t2*1000000:8.4f} - {n3:6} / {(t2-t)*1000000:8.4f}')
#    print (f'{k[0]}^{k[1]}', v['time'], v['multiplications'])
