def balance_point (arr):
    """
    Find the point in the given array, so that the sums of the left side
    and the right side are the same.

    The trick is to NOT do sum(arr[:i]) == sum(arr[i:])
    """
    ls = 0
    rs = sum(arr)

    for i in range(len(arr)):
        if ls == rs:
            return i
        else:
            ls += arr[i]
            rs -= arr[i]
    return -1


print (balance_point([1,2,3,0,7,-1])) # = 3, as 1+2+3 == 0+7-1
print (balance_point([1,2,3,0,5])) # = -1
